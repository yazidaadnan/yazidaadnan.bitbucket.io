class Carousel{

    /**
     * 
     * @param {HTMLElement} element 
     * @param {Object} options
     * @param {Object} options.slidesToScroll Nbr d'élément à faire défiler
     * @param {boolean} options.loop Doit-on boucler en fin de caroussel
     * @param {Object} options.slidesVisible Nbr d'éléments Visible en même temps 
     */
    constructor(element, options={}){
        this.element = element;
        this.options = Object.assign({},{
            slidesToScroll: 1,
            slidesVisible: 1 ,
            loop:false
        }, options);
        let children = [].slice.call(element.children);
        this.isMobile =false;
        this.currentItem = 0;
        this.root = this.createDivWithClass('carousel');
       this.container = this.createDivWithClass('carousel__container');
       this.root.appendChild(this.container);
       this.element.appendChild(this.root);
       this.moveCallbacks =[];
       this.items = children.map((child)=>{
           let item = this.createDivWithClass('carousel__item');
           item.appendChild(child);
           this.container.appendChild(item);
           return item;
       });
       this.setStyle();
       this.createNavigation();
       this.moveCallbacks.forEach(cb=> cb(0));
       this.onWindowResize();
       window.addEventListener('resize',this.onWindowResize.bind(this));
    }
    /**
     * Applique les bonne dimensions au éléments du carousel
     */
    setStyle (){
        let ratio = this.items.length / this.slidesVisible;
        this.container.style.width = (ratio*100)+ "%";
        this.items.forEach(item => item.style.width= (100/this.slidesVisible)/ratio + "%");
    }

    createNavigation(){
        let nextButton = this.createDivWithClass('carousel__next');
        let prevButton = this.createDivWithClass('carousel__prev');
        this.root.appendChild(nextButton);
        this.root.appendChild(prevButton);
        nextButton.addEventListener('click', this.next.bind(this));
        prevButton.addEventListener('click', this.prev.bind(this));
        if(this.options.loop===true){
            return;
        }
        this.onMove(index=>{
            if(index===0){
                prevButton.classList.add('carousel__prev--hidden')
            }else{
                prevButton.classList.remove('carousel__prev--hidden')
            }

            if(this.items[this.currentItem+this.slidesVisible]===undefined){
                nextButton.classList.add('carousel__next--hidden')
            }else{
                nextButton.classList.remove('carousel__next--hidden')
            }
        })
    }

    next(){
        this.gotoItem(this.currentItem + this.slidesToScroll);
      
    }

    prev(){
        this.gotoItem(this.currentItem - this.slidesToScroll);
     
    }

    /**
     * Déplace le carousel vers l'élement ciblé
     * @param {number} index 
     */
    gotoItem(index){
        if(index<0){
            index= this.items.length - this.slidesVisible;
        }else if(index >= this.items.length||(this.items[this.currentItem + this.slidesVisible]===undefined && index>this.currentItem)){
            index=0;
        }
        let translateX = index * (-100/this.items.length);
        this.container.style.transform ='translate3d(' + translateX + '%,0,0)' ;
        this.currentItem = index;
        this.moveCallbacks.forEach(cb=> cb(index));
        
    }

    onMove(cb){
        this.moveCallbacks.push(cb);
    }

    onWindowResize(){
        let mobile = window.innerWidth<800;

        if(mobile !==this.isMobile){
            this.isMobile=mobile;
            this.setStyle();
            this.moveCallbacks.forEach(cb => cb(this.currentItem));
        }
    }

    /**
     * 
     * @param {string} className
     * @returns {HTMLElement} 
     */
    createDivWithClass(className){
        let div = document.createElement('div');
        div.setAttribute('class', className);
        return div;
    }

    /**
     * @returns {number}
     */
    get slidesToScroll(){
        return this.isMobile ? 1 : this.options.slidesToScroll
    }

     /**
     * @returns {number}
     */
    get slidesVisible(){
        return this.isMobile ? 1 : this.options.slidesVisible
    }
}
document.addEventListener('DOMContentLoaded', function(){
    
    new Carousel(document.querySelector('#carousel1'),{
        slidesToScroll: 1,
        slidesVisible: 2,
        loop :false
    });
})
